public class NoodleCook {
    public static void main(String[] args) {
        Noodle waiwai =new Noodle("ไวไว", "ดั้งเดิม");
        waiwai.doFried();
        Noodle nissin= new Noodle("นิชชิน","ยากิโซบะซอสญี่ปุ่น(แบบแห้ง)");
        nissin.doDry();
        Noodle mama = new Noodle("มาม่า","ต้มยำกุ้ง");
        mama.doBoiled();
    }
}
