public class Noodle {
    String brand;
    String flavor;
    public Noodle(String brand, String flavor){
        this.brand=brand;
        this.flavor=flavor;
    }
    void doBoiled(){
        System.out.println(brand+" "+flavor+" นำไปต้ม");
    }
    void doDry(){
        System.out.println(brand+" "+flavor+" นำไปทำแบบแห้ง");
    }
    void doFried(){
        System.out.println(brand+" "+flavor+" นำไปผัด");
    }
    void doYum(){
        System.out.println(brand+" "+flavor+" นำไปยำ");
    }
}
